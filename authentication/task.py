from celery import shared_task
from django.template.loader import render_to_string
from authentication.models import B2CUser,Otp
from authentication.utils import id_generator
from authentication.service import send_email

@shared_task
def trigger_email(user,otp):
   user = B2CUser.objects.get(id=user)
   email_data={
      "email":user.email,
      "subject":"ACTIVATE YOUR ACCOUNT",
      "message":"activate your account",
      "username":user.name,
      "otp":otp
   }
   mail_html = render_to_string('authentication/activation.html', {'username':email_data["username"],'otp':email_data["otp"]})
   email_content = {
      'message': email_data["message"], 'to_adder': [email_data["email"]],
      'subject': email_data["subject"],
      'mail_html': mail_html,'from_addr': 'chsanthosha907@gmail.com',
   }
   send_email(email_content)



@shared_task
def activaion_mailer(user):
   user_data = B2CUser.objects.filter(id=user)

   otp_data = Otp.objects.filter(user=user_data.first(),otp_for="ACTIVATION")

   if otp_data.exists():
      otp_data.delete()

   otp = id_generator()

   otp_save = Otp.objects.create(otp=otp,otp_for="ACTIVATION",user=user_data.first())
   trigger_email.delay(user=user_data.first().id,otp=otp)
   return {"status":"Activation Mailer Sent."}


@shared_task
def post_user_create(user):
   user_data = B2CUser.objects.filter(id=user)
   user_update = user_data.update(is_active=False)
   activaion_mailer.delay(user)
   return {"status":"User Status Updated."}


@shared_task
def agent_sign_up_email(user):
   user = B2CUser.objects.get(id=user)
   update_agent = B2CUser.objects.filter(id=user.id).update(is_agent=True,is_active=False)
   email_data={
      "email":user.email,
      "subject":"ACCOUNT WILL BE APPROVED SOON",
      "message":"ACCOUNT WILL BE APPROVED SOON",
      "username":user.name
   }
   mail_html = render_to_string('authentication/agent_email.html', {'username':email_data["username"]})
   email_content = {
      'message': email_data["message"], 'to_adder': [email_data["email"]],
      'subject': email_data["subject"],
      'mail_html': mail_html,'from_addr': 'chsanthosha907@gmail.com',
   }
   send_email(email_content)


@shared_task
def password_reset_mailer(user):

   user_data = B2CUser.objects.filter(id=user)

   otp_data = Otp.objects.filter(user=user_data.first(),otp_for="RESET")

   if otp_data.exists():
      otp_data.delete()

   otp = id_generator()

   otp_save = Otp.objects.create(otp=otp,otp_for="RESET",user=user_data.first())

   email_data={
      "email":user_data.first().email,
      "subject":"RESET YOUR PASSWORD",
      "message":"RESET YOUR PASSWORD",
      "username":user_data.first().name,
      "otp":otp_save.otp
   }
   mail_html = render_to_string('authentication/password_reset.html', {'username':email_data["username"],'otp':email_data["otp"]})
   email_content = {
      'message': email_data["message"], 'to_adder': [email_data["email"]],
      'subject': email_data["subject"],
      'mail_html': mail_html,'from_addr': 'PLAYTONIA <support@playtonia.com>',
   }
   send_email(email_content)