from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.contrib.auth.hashers import check_password
from authentication.models import B2CUser,Otp,AgentUser

class RegisterSerializer(serializers.Serializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=B2CUser.objects.all())]
    )
    password = serializers.CharField(required=True,min_length=8, write_only=True)

    name=serializers.CharField(required=True)

    phone = serializers.CharField(required=True)

    role = serializers.CharField(required=True)


    def create(self, validated_data):
        user = B2CUser.objects.create_user(**validated_data)
        return user


class VerifySerializer(serializers.Serializer):
    email=serializers.EmailField(required=True)

    otp=serializers.CharField(required=True,min_length=6)

    def validate(self,validated_data):
        user=B2CUser.objects.filter(email=validated_data["email"])

        otp_data=Otp.objects.filter(otp=validated_data["otp"],otp_for="ACTIVATION")

        if not user.exists():
            raise serializers.ValidationError({"response":"email_not_found"})

        if not otp_data.exists():
             raise serializers.ValidationError({"otp":"Invalid Otp"})

        user.update(is_active=True)
        otp_data.delete()
        return validated_data


class LoginSerializer(serializers.Serializer):

    email = serializers.EmailField(
        required=True
    )
   
    password = serializers.CharField(
        required=True,
        min_length=8
    )

    def validate(self, validated_data):
        user = B2CUser.objects.filter(email=validated_data["email"])

        if not user.exists():
            raise serializers.ValidationError({"email":"Email not found."})
        
        if not check_password(validated_data["password"],user.first().password):
            raise serializers.ValidationError({'password':"Password Incorrect"})
        
        if not user.first().is_active and user.first().is_agent:
            raise serializers.ValidationError({"response":"Wait for the admin to approve your account"})
        
        if not user.first().is_active:
            raise serializers.ValidationError({"response":"account_not_active"})

        return validated_data


class ResendActivationSerializer(serializers.Serializer):

    email = serializers.EmailField(
        required=True
    )

    def validate(self, validated_data):

        user = B2CUser.objects.filter(email=validated_data["email"])

        if not user.exists():
            raise serializers.ValidationError({"response":"email_not_found"})
        
        if user.first().is_active:
            raise serializers.ValidationError({"response":"user_active"})

        return validated_data

class AgentRegisterSerializer(serializers.Serializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=B2CUser.objects.all())]
    )
    password = serializers.CharField(required=True,min_length=8, write_only=True)

    name=serializers.CharField(required=True)

    phone = serializers.CharField(required=True)

    role = serializers.CharField(required=True)

    agentOrCompanyName = serializers.CharField(required=True)

    officeNo = serializers.CharField(required=True)

    companyWebsite = serializers.CharField(required=True)

    gstno = serializers.CharField(required=True)

    zipCode = serializers.CharField(required=True)

    city = serializers.CharField(required=True)

    state = serializers.CharField(required=True)

    country = serializers.CharField(required=True)


    def create(self, validated_data):
        user = B2CUser.objects.create_user(
            email = validated_data["email"],
            password = validated_data["password"],
            name = validated_data["name"],
            phone = validated_data["phone"],
            role = validated_data["role"]
        )

        if user:
            agent = AgentUser.objects.create(
                agentOrCompanyName = validated_data["agentOrCompanyName"],
                officeNo = validated_data["officeNo"],
                companyWebsite = validated_data["companyWebsite"],
                gstno = validated_data["gstno"],
                zipCode = validated_data["zipCode"],
                city = validated_data["city"],
                state = validated_data["state"],
                country = validated_data["country"],
                user = user
            )

        return validated_data


class PasswordResetSerializer(serializers.Serializer):

    email = serializers.EmailField(
        required=True
    )

    def validate(self, validated_data):
        
        user = B2CUser.objects.filter(email=validated_data["email"])

        if not user.exists():
            raise serializers.ValidationError({"response":"Email not found."})
        
        return validated_data

   
class VerifyOtpSerializer(serializers.Serializer):

    email = serializers.EmailField(
        required=True
    )

    otp = serializers.CharField(
        required=True,
        min_length=6
    )

    def validate(self, validated_data):
        
        user = B2CUser.objects.filter(email=validated_data["email"])

        if not user.exists():
            raise serializers.ValidationError({"response":"email_not_found"})
        
        if not Otp.objects.filter(user=user.first(),otp=validated_data["otp"],otp_for="RESET").exists():
            raise serializers.ValidationError({"response":"Invalid reset code"})

        return validated_data


class ChangePasswordSerializer(serializers.Serializer):

    email = serializers.EmailField(
        required=True
    )

    otp = serializers.CharField(
        required=True,
        min_length=6
    )

    password = serializers.CharField(required=True,min_length=8)

    def validate(self, validated_data):
        
        user = B2CUser.objects.filter(email=validated_data["email"])

        if not user.exists():
            raise serializers.ValidationError({"response":"email_not_found"})
        
        if not Otp.objects.filter(user=user.first(),otp=validated_data["otp"],otp_for="RESET").exists():
            raise serializers.ValidationError({"response":"otp_not_found"})

        return validated_data


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgentUser
        fields = ["agentOrCompanyName","officeNo","companyWebsite","gstno","zipCode","city","state","country"]

class AgentSerializer(serializers.ModelSerializer):
    agent = UserSerializer(many=True,source="agents")
    class Meta:
        model = B2CUser
        fields = ["id","name","phone","email","agent"]
  

class GetAgentSerializer(serializers.ModelSerializer):
    agent = UserSerializer(many=True,source="agents")
    class Meta:
        model = B2CUser
        fields = ["id","name","phone","email","agent"]