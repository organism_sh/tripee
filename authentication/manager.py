from django.contrib.auth.base_user import BaseUserManager

class CustomUserManager(BaseUserManager):
    use_in_migrations = True
    def _create_user(self,email,password,**extra_fields):
        if not email:
            raise ValueError(_('The Email must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        #user.user_id = uuid.uuid4()
        user.set_password(password)
        user.save(using=self._db)
        return user

    def _ceate_agent(self,email,password,**extra_fields):
        if not email:
            raise ValueError(_('The Email must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        #user.user_id = uuid.uuid4()
        user.set_password(password)
        user.save(using=self._db)
        return user
        
    def create_user(self,email,name,phone,role,password=None):
        return self._create_user(email,password,name=name,phone=phone,role=role)

    def create_superuser(self,email,name,phone,role,password=None):
        return self._create_user(email,password,name=name,phone=phone,role=role,is_staff=True,is_superuser=True)

    def create(self,**extra_fields):
        pass