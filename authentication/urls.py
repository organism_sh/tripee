from django.urls import path
from .import views

urlpatterns = [
    path('sign_up', views.Register, name='sign_up'),
    path('verify', views.VerifyUser, name='verify'),
    path('sign_in', views.sign_in, name='sign_in'),
    path('resend_activation', views.resend_activation, name='resend_activation'),
    path('agent_sign_up', views.AgentRegister, name='agent_sign_up'),
    path('password_reset', views.password_reset, name='password_reset'),
    path('verify_otp', views.verify_otp, name='verify_otp'),
    path('change_password', views.change_password, name='change_password'),
    path('all_agents', views.all_agents, name='all_agents'),
    path('agent/<int:id>', views.agent, name='agent')
]
