from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from authentication.manager import CustomUserManager
from django.contrib.auth.models import PermissionsMixin
from django.conf import settings


class B2CUser(AbstractBaseUser,PermissionsMixin):
   email=models.EmailField(_('email address'), unique=True)
   name=models.CharField(max_length=255,null=True)
   phone=models.CharField(max_length=255,default=False,null=True)
   role=models.CharField(max_length=255,default=False,null=True)
   is_staff = models.BooleanField(default=False)
   is_agent = models.BooleanField(default=False)
   is_active = models.BooleanField(default=True)
   is_superuser = models.BooleanField(default=False)
   date_joined = models.DateTimeField(default=timezone.now)

   USERNAME_FIELD='email'
   REQUIRED_FIELDS = ['name','phone','role']
   objects=CustomUserManager()

   def __str__(self):
      return self.name


class Otp(models.Model):
    otp = models.CharField(max_length=6)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,null=True,blank=True,on_delete=models.SET_NULL)
    otp_for = models.CharField(max_length=13,null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class AgentUser(models.Model):
   user = models.ForeignKey(settings.AUTH_USER_MODEL,null=True,blank=True,on_delete=models.SET_NULL,related_name="agents")
   agentOrCompanyName=models.CharField(max_length=255,null=True)
   officeNo=models.CharField(max_length=255,null=True)
   companyWebsite=models.CharField(max_length=255,null=True)
   gstno=models.CharField(max_length=255,null=True)
   zipCode=models.CharField(max_length=255,null=True)
   city=models.CharField(max_length=255,null=True)
   state=models.CharField(max_length=255,null=True)
   country=models.CharField(max_length=255,null=True)