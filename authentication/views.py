from django.shortcuts import render
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.decorators import api_view,permission_classes
from authentication.serializer import (
    RegisterSerializer,
    VerifySerializer,
    LoginSerializer,
    ResendActivationSerializer,
    AgentRegisterSerializer,
    PasswordResetSerializer,
    VerifyOtpSerializer,
    ChangePasswordSerializer,
    AgentSerializer,
    GetAgentSerializer
)
from authentication.task import (
    post_user_create,
    activaion_mailer,
    agent_sign_up_email,
    password_reset_mailer
    )
from django.contrib.auth import authenticate
from authentication.models import B2CUser,Otp,AgentUser
from authentication.service import generate_token

# Create your views here.

@api_view(['POST'])
@permission_classes([AllowAny])
def Register(request):
    serializer = RegisterSerializer(data=request.data)
    if serializer.is_valid():
        user=serializer.save()
        if user:
            post_user_create.delay(user.id)
            return Response(serializer.data,status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([AllowAny])
def VerifyUser(request):
    serializer = VerifySerializer(data=request.data)
    if serializer.is_valid():
        return Response(serializer.data,status=status.HTTP_200_OK)
    return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([AllowAny])
def sign_in(request):
    serializer = LoginSerializer(data=request.data)
    if serializer.is_valid():
        user = B2CUser.objects.get(email=serializer.data["email"])
        user_auth = authenticate(email=user.email, password=serializer.data["password"])
        if user_auth:
            authenticated = generate_token(user=user)
        return Response(authenticated, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([AllowAny])
def resend_activation(request):
    serializer = ResendActivationSerializer(data=request.data)
    if serializer.is_valid():
        user = B2CUser.objects.get(email=serializer.data["email"])
        activaion_mailer.delay(user.id)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([AllowAny])
def AgentRegister(request):
    serializer = AgentRegisterSerializer(data=request.data)
    if serializer.is_valid():
        user=serializer.save()
        user = B2CUser.objects.get(email=serializer.data["email"])
        if user:
            agent_sign_up_email.delay(user.id)
            return Response(serializer.data,status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([AllowAny])
def password_reset(request):
    serializer = PasswordResetSerializer(data=request.data)
    if serializer.is_valid():
        user = B2CUser.objects.get(email=serializer.data["email"])
        password_reset_mailer.delay(user.id)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([AllowAny])
def verify_otp(request):
    serializer = VerifyOtpSerializer(data=request.data)
    if serializer.is_valid():
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([AllowAny])
def change_password(request):
    serializer = ChangePasswordSerializer(data=request.data)
    if serializer.is_valid():
        user = B2CUser.objects.get(email=serializer.data["email"])
        user.set_password(serializer.data["password"])
        user.save()
        otp = Otp.objects.filter(user=user,otp=serializer.data["otp"],otp_for="RESET")
        otp.delete()
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
@permission_classes([AllowAny])
def all_agents(request):
    agents = B2CUser.objects.filter(is_agent=True)
    serializer = AgentSerializer(agents,many=True)
    if serializer:
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST) 



@api_view(['GET'])
@permission_classes([AllowAny])
def agent(request,id):
    agent = B2CUser.objects.filter(id=id)
    if not agent.exists():
        return Response({"response":"not_found"}, status=status.HTTP_400_BAD_REQUEST)
    serializer = GetAgentSerializer(agent.first())
    if serializer:
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)