
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _

from .models import B2CUser,Otp,AgentUser


@admin.register(B2CUser)
class UserAdmin(DjangoUserAdmin):
    """Define admin model for custom User model with no email field."""

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('name',)}),
        (_('Permissions'), {'fields': ('is_active','is_agent', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('date_joined',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'name', 'is_staff','is_agent')
    search_fields = ('email', 'name')
    ordering = ('email',)



class AgentUserAdmin(admin.ModelAdmin):
    raw_id_fields = ('user',)
    search_fields = ('id', 'user')
    list_display = ('id', 'user')
admin.site.register(AgentUser,AgentUserAdmin)

class OtpAdmin(admin.ModelAdmin):
    raw_id_fields = ('user',)
    search_fields = ('otp', 'user')
    list_display = ('otp', 'user')
admin.site.register(Otp,OtpAdmin)